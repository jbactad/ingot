// package login handles communication with the client when the connection is in the login state.
// Thus, it handles authentication with minecraft servers and protocol encryption, if online.
package login

import (
	"bytes"
	"fmt"
	"gitlab.com/ingotmc/ingot/server"
	"gitlab.com/ingotmc/packet"
	"gitlab.com/ingotmc/protocol"
)

type loginInfo struct {
	username string
	token    []byte
}

// Authenticator handles authentication against minecraft servers and protocol encryption.
// It does not change the connection state to protocol.Play after a successful login.
type Authenticator struct {
	kh       *KeyHelper
	callback func(conn *server.Connection, username, uuid string)
	clients  map[*server.Connection]loginInfo
}

// NOTE: might need to change the constructor to accept a generic ConnectionEncrypter interface
// NewAuthenticator returns a new Authenticator with kh as the underlying helper for protocol encryption.
// kh may be nil if it's possible to guarantee that every server.Connection the Authenticator will handle will be offline.
func NewAuthenticator(kh *KeyHelper) *Authenticator {
	return &Authenticator{
		kh: kh,
	}
}

// NewAuthenticatorWithCallback returns an Authenticator which calls the specified function upon successful completion
// of the login flow.
func NewAuthenticatorWithCallback(kh *KeyHelper, callback func(conn *server.Connection, username, uuid string)) *Authenticator {
	return &Authenticator{
		kh:       kh,
		callback: callback,
	}
}

func (a *Authenticator) Handle(p protocol.Packet, conn *server.Connection) {
	if a.clients == nil {
		a.clients = make(map[*server.Connection]loginInfo)
	}
	if _, ok := a.clients[conn]; !ok {
		a.clients[conn] = loginInfo{}
	}
	go a.handle(p, conn)
}

func (a *Authenticator) handle(p protocol.Packet, conn *server.Connection) {
	switch np := p.(type) {
	case *packet.LoginStart:
		a.handleLoginStart(np, conn)
	case *packet.EncryptionResponse:
		a.handleEncryptionRequest(np, conn)
	default:
		fmt.Println("unhandled packet with id", p.ID(true), "in state login")
	}
}

func (a *Authenticator) sendLoginSuccess(conn *server.Connection, username, uuid string) {
	if a.callback != nil {
		defer a.callback(conn, username, uuid)
	}
	conn.SendPacket(packet.NewLoginSuccess(uuid, username))
}

func (a *Authenticator) handleEncryptionRequest(encResp *packet.EncryptionResponse, conn *server.Connection) {
	token, err := a.kh.Decrypt(encResp.VerifyToken)
	if err != nil {
		fmt.Println("decrypt error")
	}
	if !bytes.Equal(token, a.clients[conn].token) {
		fmt.Println(ErrTokenMismatch)
		return
	}
	if err != nil {
		fmt.Println("error getting keypair")
		return
	}
	sharedSecret, _ := a.kh.Decrypt(encResp.SharedSecret)
	hash := genLoginHash(sharedSecret, a.kh.PubKey())
	uuid, err := authMojang(a.clients[conn].username, hash)
	if err != nil {
		fmt.Println("error authenticating", err)
		return
	}
	err = encryptConnection(sharedSecret, conn)
	if err != nil {
		fmt.Println("error connecting")
		return
	}
	a.sendLoginSuccess(conn, a.clients[conn].username, formatUUID(uuid))
}

func (a *Authenticator) handleLoginStart(lStart *packet.LoginStart, conn *server.Connection) {
	fmt.Println("new connection with username", lStart.Username)
	if conn.Offline {
		a.sendLoginSuccess(conn, string(lStart.Username), formatUUID(genOfflineUUID()))
		return
	}
	if a.kh == nil {
		// TODO: let the caller know about the error somehow
		return
	}
	token := genVerifyToken()
	a.clients[conn] = loginInfo{
		username: string(lStart.Username),
		token:    token,
	}
	conn.SendPacket(packet.NewEncryptionRequest(a.kh.PubKey(), token))
}
