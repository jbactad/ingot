package login

import (
	"crypto/aes"
	"crypto/rand"
	"crypto/sha1"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/ingotmc/cfb8"
	"gitlab.com/ingotmc/ingot/server"
	"io"
	"net/http"
	"strings"
)

const (
	mojangAPIBaseURL = "https://sessionserver.mojang.com/session/minecraft/hasJoined?username=%s&serverId=%s"
)

var (
	ErrTokenMismatch = errors.New("verify token received by client differs from generated one")
)

// TODO: check authentication response
func authMojang(username, hash string) (UUID string, err error) {
	url := fmt.Sprintf(mojangAPIBaseURL, username, hash)
	resp, err := http.Get(url)
	if err != nil {
		return
	}
	respJson := struct {
		ID string
	}{}
	err = json.NewDecoder(resp.Body).Decode(&respJson)
	UUID = respJson.ID
	return
}

// FIXME: this currently doesn't work, if we encrypt the connection we receive corrupted data
func encryptConnection(key []byte, conn *server.Connection) error {
	block, err := aes.NewCipher(key)
	if err != nil {
		return err
	}
	encStream := CFB8.NewCFB8Encrypt(block, key)
	decStream := CFB8.NewCFB8Decrypt(block, key)
	conn.SetCipher(encStream, decStream)
	return nil
}

//from https://gist.github.com/toqueteos/5372776
func genOfflineUUID() string {
	b := make([]byte, 16)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

func genVerifyToken() (b []byte) {
	b = make([]byte, 4)
	rand.Read(b)
	return
}

func formatUUID(s string) string {
	// format: 11111111-2222-3333-4444-555555555555
	if len(s) != 32 {
		return s
	}
	return fmt.Sprintf("%s-%s-%s-%s-%s", s[0:8], s[8:12], s[12:16], s[16:20], s[20:32])
}

func genLoginHash(sharedSecret []byte, pubKey []byte) string {
	h := sha1.New()
	io.WriteString(h, "")
	h.Write(sharedSecret)
	h.Write(pubKey)
	hash := h.Sum(nil)
	// Check for negative hashes
	negative := (hash[0] & 0x80) == 0x80
	if negative {
		hash = twosComplement(hash)
	}

	// Trim away zeroes
	res := strings.TrimLeft(fmt.Sprintf("%x", hash), "0")
	if negative {
		res = "-" + res
	}

	return res
}

// little endian
func twosComplement(p []byte) []byte {
	carry := true
	for i := len(p) - 1; i >= 0; i-- {
		p[i] = byte(^p[i])
		if carry {
			carry = p[i] == 0xff
			p[i]++
		}
	}
	return p
}
