package main

import (
	"fmt"
	"gitlab.com/ingotmc/ingot/login"
	"gitlab.com/ingotmc/ingot/server"
	"gitlab.com/ingotmc/packet"
	"gitlab.com/ingotmc/protocol"
	"gitlab.com/ingotmc/protocol/codec"
	"log"
	"net"
)

func errFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	ln, err := net.Listen("tcp", ":25565")
	errFatal(err)
	srv := server.New(ln)
	//wrld := world.New()
	srv.HandleFunc(protocol.Handshaking, func(p protocol.Packet, conn *server.Connection) {
		switch np := p.(type) {
		case *packet.Handshake:
			fmt.Println("new connection, protocol version", np.ProtocolVersion)
			conn.State = protocol.Login
		default:
			fmt.Printf("unhandled packet with id %d in state Handshaking\n", p.ID(true))
		}
	})
	authenticator := login.NewAuthenticatorWithCallback(
		login.NewKeyHelper("server.key"),
		func(conn *server.Connection, username, uuid string) {
			conn.State = protocol.Play
			conn.SendPacket(&packet.JoinGame{
				EntityID:         23,
				Gamemode:         0,
				Dimension:        0,
				MaxPlayers:       3,
				LevelType:        "default",
				ViewDistance:     8,
				ReducedDebugInfo: false,
			})
			fmt.Println("sent joingame")
			conn.SendPacket(&packet.HeldItemChange{
				Slot: 2,
			})
			conn.SendPacket(&packet.SpawnPosition{
				Location: codec.Position{
					X: 64,
					Y: 64,
					Z: 64,
				},
			})
			//fmt.Println("sent spawnpoint")
			conn.SendPacket(&packet.PlayerPositionAndLook{
				X:          0,
				Y:          64,
				Z:          0,
				Yaw:        0,
				Pitch:      0,
				Flags:      0,
				TeleportID: 1611,
			})
			//fmt.Println("sent posandlook")
		})
	srv.Handle(protocol.Login, authenticator)
	srv.HandleFunc(protocol.Play, func(p protocol.Packet, conn *server.Connection) {
		fmt.Println("received packet id", p.ID(true), "in state play")
	})
	fmt.Println("starting")
	srv.Listen()
}
