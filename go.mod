module gitlab.com/ingotmc/ingot

go 1.14

require (
	gitlab.com/ingotmc/cfb8 v0.1.1
	gitlab.com/ingotmc/packet v0.1.1
	gitlab.com/ingotmc/protocol v0.1.0
)
